package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 系统启动器
 * 作者：凌景标
 * 时间：2019年4月17日下午3:23:42
 *
 */
@SpringBootApplication
@ServletComponentScan
@EnableAutoConfiguration
public class StartApplication  extends SpringBootServletInitializer{
	 
	 @Override
		protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
			return application.sources(StartApplication.class);
		}
		
		public static void main(String[] args) {
			SpringApplication.run(StartApplication.class, args);
		}
}
