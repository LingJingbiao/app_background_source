package com.utils.operation;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * io操作工具
 * 作者：凌景标
 * 时间：2019年4月25日上午10:11:13
 *
 */
public final class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 获取类加载器
     */
    public static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    /**
     * 加载类
     */
    public static Class<?> loadClass(String className, boolean isInitialized) {
        Class<?> cls;
        try {
            cls = Class.forName(className, isInitialized, getClassLoader());
        } catch (ClassNotFoundException e) {
            LOGGER.error("load class failure", e);
            throw new RuntimeException(e);
        }
        return cls;
    }

    /**
     * 加载类（默认将初始化类）
     */
    public static Class<?> loadClass(String className) {
        return loadClass(className, true);
    }
    /**
     * 获取packpath下的所有包类。慢
     * @param packpath
     * @return
     */
    public  static List<String> getClassName(String packpath){
    	long start=System.nanoTime();
    	List<String> list=new ArrayList<String>();
		try {
			  Enumeration<URL> urls = getClassLoader().getResources(packpath.replace(".", "/"));
			    while (urls.hasMoreElements()) {
			        URL url = urls.nextElement();
			        if (url != null) {
	                    String protocol = url.getProtocol();
	                    if (protocol.equals("file")) {
	                        String packagePath = url.getPath().replaceAll("%20", " ");
	                        list= addClassName(packagePath, packpath);
	                    }
	                }
			       
			    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		long end=System.nanoTime();
		System.out.println("程序运行时间： "+(end-start)+"ms");
		return list;
		
	}
    /**
     * 根据包获取路径 速度快（推荐）（效果getClassName方法一样）
     * @param packpath
     * @return
     */
    public  static List<String> getPathName(String packpath){
    	long start=System.nanoTime();
    	List<String> list=new ArrayList<String>();
    	String path="src\\main\\java\\"+packpath.replace(".", "/");
		try {
			File file=new File(path);
			File[] files= file.listFiles();
			for(File ofile:files){
				list.add(ofile.getName().substring(0,ofile.getName().length()-5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		long end=System.nanoTime();
		System.out.println("程序运行时间： "+(end-start)+"ms");
		return list;
		
	}
	
    public static List<String> addClassName( String packagePath, String packageName) {
    	List<String> list=new ArrayList<String>();
        File[] files = new File(packagePath).listFiles(new FileFilter() {
            public boolean accept(File file) {
                return (file.isFile() && file.getName().endsWith(".class")) || file.isDirectory();
            }
        });
        for (File file : files) {
            String fileName = file.getName();
            if (file.isFile()) {
                String className = fileName.substring(0, fileName.lastIndexOf("."));
                list.add(className);
            } 
        }
		return list;
    }
    /**
     * 根据当前项目路径判断是否有文件
     * @param packpath
     * @return
     */
    public  static boolean exists(String packpath){
    	boolean res=false;
    	String path="src\\main\\java\\"+packpath;
		try {
			File file=new File(path);
			res=file.exists();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
		
	}
    
}








