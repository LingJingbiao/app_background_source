package com.utils.create;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Modifier;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.utils.operation.FileUtil;

/**
 * 根据实体类生成代码
 * 作者：凌景标
 * 时间：2019年4月25日上午10:30:28
 * 版本：1.0
 */
public class Generatecode {
	
	 public static void main(String[] args) {
		 String packpath="com.base.sys.entity";//实体类所在的包路径
		String scpath= packpath.substring(0, packpath.length()-6);
		 List<String> namelist=FileUtil.getClassName(packpath);
		 for(String name:namelist){
			 createServiceClass(scpath, name);
			 createServiceImplClass(scpath, name);
			 createDaoClass(scpath, name); 
			 createControllerClass(scpath, name);
		 }
	 }
/**
 * 生成service接口（根据实体类名称生成）
 * @param packpath 实体类路径
 * @param mName 实体类名称
 */
 public static void createServiceClass(String packpath,String mName){
	 String filepath=packpath.replace(".", "/")+"service\\"+mName+"Service.java";
	 boolean exists=FileUtil.exists(filepath);
	 if(!exists){
		 ClassName model = ClassName.get(packpath+"entity", mName);
		 ClassName extendsClass = ClassName.get("com.base.config.service", "BaseService");
		 //创建名称、 继承、接口
		 TypeSpec.Builder mainActivityBuilder = TypeSpec.interfaceBuilder(mName+"Service")
				 .addModifiers(Modifier.PUBLIC)
				 .addSuperinterface(extendsClass);
	//   创建方法
		   MethodSpec creategetModel = MethodSpec.methodBuilder("get"+mName)
				 .addModifiers(Modifier.ABSTRACT,Modifier.PUBLIC) 
		  		 .returns(model)
		  		 .addParameter(model,"model")
		         .build();
		   
		 //创建上面方法
		 TypeSpec mainActivity = mainActivityBuilder
				 .addMethod(creategetModel)
				 .build();
		 JavaFile file = JavaFile.builder(packpath+"service", mainActivity).build();
		 
		 try {
			 System.out.println("service生成成功");
			 file.writeTo(new File("src\\main\\java"));
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	 }else{
		 System.out.println(mName+"Service.java已存在");
	 }
 
 }
 /**
  * 生成Serviceimpl实现类
  * @param packpath
  * @param mName
  */
 public static void createServiceImplClass(String packpath,String mName){
	 String filepath=packpath.replace(".", "/")+"service\\impl\\"+mName+"ServiceImpl.java";
	 boolean exists=FileUtil.exists(filepath);
	 if(!exists){
     ClassName interfaceClass = ClassName.get(packpath+"service",mName+"Service");//导入接口包
     ClassName privateDao = ClassName.get(packpath+"dao", mName+"Dao");
     ClassName model = ClassName.get(packpath+"entity", mName);
     ClassName extendsClass = ClassName.get("com.base.config.service.impl", "BaseServiceImpl");
     ClassName Service = ClassName.get("org.springframework.stereotype", "Service");
     ClassName Autowired = ClassName.get("org.springframework.beans.factory.annotation", "Autowired");
    
     //创建名称、 继承、接口
     TypeSpec.Builder mainActivityBuilder = TypeSpec.classBuilder(mName+"ServiceImpl")
    		 .addAnnotation(Service)
             .addModifiers(Modifier.PUBLIC)
             .superclass(extendsClass)
             .addSuperinterface(interfaceClass);
     //创建实例化对象
     FieldSpec spec = FieldSpec.builder(privateDao, "modelDao").addAnnotation(Autowired)
    		 .addModifiers(Modifier.PRIVATE)
    		 .build();

//   创建方法
		   MethodSpec creategetModel = MethodSpec.methodBuilder("get"+mName)
				 .addModifiers(Modifier.PUBLIC) 
		  		 .returns(model)
		  		 .addParameter(model,"model")
// 		  		 .addStatement("$T pramModel = new $T()",model)
		  		 .addStatement(mName+" pramModel=new "+mName+"()")
		  		 
		  		 .addCode("try {")
		  		 .addStatement("pramModel= ("+mName+") super.queryModel(model)")
		  		 .addCode("} catch (Exception e) {")
		  		 .addStatement("e.printStackTrace()")
		  		 .addCode("}")
		  		 .addStatement("return pramModel")
		         .build();

     //实现上面方法
     TypeSpec mainActivity = mainActivityBuilder.addField(spec)
    		 .addMethod(creategetModel)
             .build();
     JavaFile file = JavaFile.builder(packpath+"service.impl", mainActivity).build();

     try {
    	  System.out.println("serviceImpl生成成功");
    	  file.writeTo(new File("src\\main\\java"));
     } catch (IOException e) {
         e.printStackTrace();
     }
	 }else{
		 System.out.println(mName+"ServiceImpl.java已存在");
	 }
 
 }
 /**
  * 生成dao类
  * @param packpath
  * @param mName
  */
 public static void createDaoClass(String packpath,String mName){
	 String filepath=packpath.replace(".", "/")+"dao\\"+mName+"Dao.java";
	 boolean exists=FileUtil.exists(filepath);
	 if(!exists){
     ClassName Mapper = ClassName.get("org.apache.ibatis.annotations", "Mapper");
     //创建名称、 继承、接口
     TypeSpec.Builder mainActivityBuilder = TypeSpec.interfaceBuilder(mName+"Dao")
    		 .addAnnotation(Mapper)
             .addModifiers(Modifier.PUBLIC);
     //实现上面方法
     TypeSpec mainActivity = mainActivityBuilder
             .build();
     JavaFile file = JavaFile.builder(packpath+"dao", mainActivity).build();

     try {
    	  System.out.println("dao生成成功");
    	  file.writeTo(new File("src\\main\\java"));
     } catch (IOException e) {
         e.printStackTrace();
     }
	 }else{
		 System.out.println(mName+"Dao.java已存在");
	 }
 }
 /**
  * 生成Controller
  * @param packpath
  * @param mName
  */
 public static void createControllerClass(String packpath,String mName){
	 String filepath=packpath.replace(".", "/")+"controller\\"+mName+"Controller.java";
	 boolean exists=FileUtil.exists(filepath);
	 if(!exists){
     ClassName Autowired = ClassName.get("org.springframework.beans.factory.annotation", "Autowired");
     ClassName Controller = ClassName.get("org.springframework.stereotype", "Controller");
     
     ClassName RequestMapping = ClassName.get("org.springframework.web.bind.annotation", "RequestMapping");
     ClassName ResponseBody = ClassName.get("org.springframework.web.bind.annotation", "ResponseBody");
     ClassName Pages = ClassName.get("com.base.config.entity", "Pages");
     ClassName model = ClassName.get(packpath+"entity", mName);
     ClassName service = ClassName.get(packpath+"service",mName+"Service");
     
     AnnotationSpec sc= AnnotationSpec.builder(RequestMapping).addMember("value", "\"mb/"+mName.toLowerCase()+"\"").build();
     //创建名称、 继承、接口
     TypeSpec.Builder mainActivityBuilder = TypeSpec.classBuilder(mName+"Controller")
    		 .addJavadoc("$L", "代码生成器")
    		 .addAnnotation(Controller)
    		 .addAnnotation(sc)
             .addModifiers(Modifier.PUBLIC);
     //创建实例化对象
     FieldSpec spec = FieldSpec.builder(service, "controllerService").addAnnotation(Autowired)
    		 .addModifiers(Modifier.PRIVATE)
    		 .build();
//     创建方法
     AnnotationSpec savesc= AnnotationSpec.builder(RequestMapping).addMember("value", "\"save\"").build();
     MethodSpec createSave = MethodSpec.methodBuilder("saveorupdate")
    		 .addAnnotation(savesc)
    		 .addAnnotation(ResponseBody)
    		 .addModifiers(Modifier.PUBLIC)
    		 .returns(Map.class)
    		 .addParameter(model,"model")
    		 .addStatement("Map res=controllerService.saveorupdate(model)")
    		 .addStatement("return res")
             .build();
     
     AnnotationSpec listsc= AnnotationSpec.builder(RequestMapping).addMember("value","\"pageList\"").build();
     MethodSpec createPageList = MethodSpec.methodBuilder("pageList")
    		 .addAnnotation(listsc)
    		 .addAnnotation(ResponseBody)
    		 .addModifiers(Modifier.PUBLIC)
    		 .returns(Pages)
    		 .addParameter(model, "model")
    		 .addParameter(Pages, "pages")
    		 .addStatement("pages=controllerService.PageList(model, pages)")
    		 .addStatement("return pages")
             .build();
     
     AnnotationSpec deletesc= AnnotationSpec.builder(RequestMapping).addMember("value","\"delete\"").build();
     MethodSpec createDelete = MethodSpec.methodBuilder("delete")
    		 .addAnnotation(deletesc)
    		 .addAnnotation(ResponseBody)
    		 .addModifiers(Modifier.PUBLIC)
    		 .returns(String.class)
    		 .addParameter(model, "model")
    		 .addParameter(String.class, "ids")
    		 .addStatement("String res=controllerService.delete(model, ids)")
    		 .addStatement("return res")
             .build();
     //实现上面方法
     TypeSpec mainActivity = mainActivityBuilder.addMethod(createSave)
    		 .addMethod(createPageList)
    		 .addMethod(createDelete)
    		 .addField(spec)
             .build();
     JavaFile file = JavaFile.builder(packpath+"controller", mainActivity).build();

     try {
    	  System.out.println("controller生成成功");
         file.writeTo(new File("src\\main\\java"));
     } catch (IOException e) {
         e.printStackTrace();
     }
	 }else{
		 System.out.println(mName+"Controller.java已存在");
	 }
 }

 
 
 

}