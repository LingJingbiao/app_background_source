package com.utils.create;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.base.config.entity.Pages;
import com.base.config.entity.ReturnMessage;
import com.utils.base.BaseUtil;
import com.utils.operation.TimeUtils;

/**
 * 增、删、改、查sql生成工具类
 * @author ljb
 * @date 2019年4月16日
 *
 */
public class CreateSqlUtils {
	/**
	 * 保存或更新sql生成
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public static Map<String,String> saveorUpdateSql(Object model) throws Exception{
		Map<String,String> map=new HashMap<String,String>();
		ReturnMessage mes=new ReturnMessage();
		Class clazz = model.getClass();
		String sqltype="";
		String sqls="";
		String zd1 = "";//字段1
		String zd2= "";//字段2
		String clName="";//实体名
		String wherestr="";//更新条件
			 clName = clazz.getSimpleName();
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				String name =field.getName();
				Method m = (Method) clazz.getMethod("get" + getMethodName(name));
				String val = (String) m.invoke(model);
				if("id".equals(name)){
					if(val==null){
						val=BaseUtil.getUUID();
						map.put("id", val);
						sqltype="1";
					}else{
						wherestr=val;
					}
				}
				if("token".equals(name)&&val==null){
						val=BaseUtil.getUUID();
				}
				if("createDate".equals(name)&&val==null){
					val=TimeUtils.getNowDateYYYY_MM_DD_HH_MM_SS();
				}
				String namex =toCharacter(name);//修改后的字段
				if(val!=null){
					zd1 += namex +",";
					zd2 +=val+",";
				}
			}
			if("1".equals(sqltype)){
				String str =zd1.substring(0, zd1.length() -1);
				String str2 = zd2.substring(0, zd2.length() -1);
				String[] sz2=str2.split(",");
				String sq="";
				for(int i=0;i<sz2.length;i++){
					sq+="'"+sz2[i]+"'";
					if(i<sz2.length-1){
						sq+=",";
					}
				}
				sqls="insert into "+clName+" ("+str+") values("+sq+")";
			}else{
				String[] sz=zd1.split(",");
				String[] sz2=zd2.split(",");
				String sq="";
				for(int i=0;i<sz.length;i++){
					sq+=sz[i]+"='"+sz2[i]+"'";
					if(i<sz.length-1){
						sq+=",";
					}
				}
				String udate=TimeUtils.getNowDateYYYY_MM_DD_HH_MM_SS();
				sqls="update "+clName+" set "+sq+",update_Date="+udate+" where id="+wherestr;
				map.put("id", wherestr);
			}
			map.put("sql", sqls);
		
		return map;
	}

	public String delete(Object model, String ids) {
		
		String sql = "";
		return sql;
	}
	/**
	 * 分页查询
	 * @param model
	 * @param pages
	 * @return
	 * @throws Exception
	 */
	public static String queryList(Object model, Pages<Object> pages) throws Exception{
		Class clazz = model.getClass();
		
		String clName = clazz.getSimpleName();
		String Fields=getFields(model);
		String sql = "Select "+Fields+" from " + clName + " where 1=1 ";
		String querySql = "";
		String limitSql="";
		if(pages!=null){
			Map<String, Object> map = pages.getParam();
			if (map != null && map.size() > 0) {
				for (Map.Entry<String, Object> entry : map.entrySet()) {
					querySql += " and " + entry.getKey() + " ='" + entry.getValue()
							+ "' ";// 条件查询
				}
			}
			limitSql=" LIMIT " + pages.getPage() + ","+ pages.getTotalPage();
		}
		String objquerySql=objquerySql(model);//根据对象来拼接成条件
		sql += querySql+objquerySql+limitSql ;

		return sql;
	}
	/**
	 * 获取总记录数
	 * @param model
	 * @param pages
	 * @return
	 * @throws Exception
	 */
	public static String getCount(Object model, Pages<Object> pages) throws Exception{
		Class clazz = model.getClass();
		  String clName=clazz.getSimpleName();
		  String sql = "SELECT count(1) as totalCount FROM (Select * from "+clName +" where 1=1 ";
		  String querySql="";
		  Map<String, Object> map=pages.getParam();
		  if(map!=null&&map.size()>0){
			  for (Map.Entry<String, Object> entry : map.entrySet()) { 
				 querySql+=" and "+entry.getKey()+"='"+entry.getValue()+"' ";//条件查询
			  }
		  }
		  sql+=querySql+" limit "+pages.getPage()+","+pages.getTotalPage()+" ) model";
		return sql;
	}

	public static String getFields(Object model) throws Exception{
		String names="";
		Class clazz = model.getClass();
		Field[] fields = clazz.getDeclaredFields();
		int f=0;
		for (Field field : fields) {
			f++;
			String name =field.getName();
			 names +=getfield(name);//修改后的字段
			 if(f<fields.length){
				 names+=",";
			 }
			
		}
		return names;
		
	}
	private static String getfield(String str) {
		String fields="";
		String name="";
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLowerCase(c)){
				name+="_"+c;
			}else{
				name+=c;
			}
		}
		fields=name+" AS "+str;
		return fields;
	}
	/**
	 * 把一个字符串的第一个字母大写、效率是最高的、
	 */
	private static String getMethodName(String fildeName) {
		byte[] items = fildeName.getBytes();
		items[0] = (byte) ((char) items[0] - 'a' + 'A');
		return new String(items);
	}
	/**
	 * 判断是否有大写字母，有就改成xxx_xxx
	 * @param str
	 * @return
	 */
	public static String toCharacter(String str) {
		String name="";
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLowerCase(c)){
				name+="_"+c;
			}else{
				name+=c;
			}
		}
		return name;
	}
	
	private static String objquerySql(Object obj) throws Exception{
		String querySql=" ";
		if(obj!=null){
		Class clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			String name =field.getName();
			System.out.println(name);
			Method m = (Method) clazz.getMethod("get" + getMethodName(name));
			String val = (String) m.invoke(obj);
			String namex =toCharacter(name);//修改后的字段
			if(val!=null){
				querySql +=" and "+ namex +"='"+val+"'";
			}
		}
		}
		return querySql;
		 
	}
	
	public static String getClazzName(Object obj){
		Class clazz = obj.getClass();
		String clName = clazz.getSimpleName();
		return clName;
	}
	
	/**
	 * 根据ids 来拼接sql
	 * @param ids
	 * @return
	 * @throws Exception 
	 */
	public static String getIdsSql(Object obj,String ids) throws Exception{
		String cname=getClazzName(obj);
		String Fields=getFields(obj);
		String sql="Select "+Fields+" from "+cname+" where and id in ("+ids+")";
//		int i=0;
//		if(ids.contains(",")){
//		   String[] idsz=ids.split(",");
//		   for(String id:idsz){
//			   i++;
//			   sql+=id;
//			   if(i<idsz.length-1){
//				   sql+=",";
//			   }
//		   }
//		}else{
//			sql+=ids;
//		}
//		sql+=")";
		return sql;
		
	}
	/**
	 * 获取对象的某个字段的值
	 * @param obj 对象
	 * @param name 需要获取对象字段的值（首字母大写，因为，例：getId来获取值）
	 * @return
	 */
	public static String getValue(Object obj,String name) throws Exception{
		Class clazz = obj.getClass();
		Method m = (Method) clazz.getMethod("get" + name);
		String val = (String) m.invoke(obj);
		return val;
		
	}

}
