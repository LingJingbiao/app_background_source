package com.utils.create;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;


/**
 * 生成xml-用于mybatis的xml
 * 作者：凌景标
 * 时间：2019年5月15日上午10:30:54
 *
 */
public class CreateXML {

	public static void main(String[] args) {
		createXml("");
	}

	/**
	 * 生成xml方法
	 */
//	public static void createXml(){
//		Document document = DocumentHelper.createDocument();
//		Element emapper = document.addElement("mapper");  
//		 emapper.addAttribute("namespace","com.sn.order.dao.LogisDao");
//		 
//		 OutputFormat format = OutputFormat.createCompactFormat();
//		 StringWriter writer = new StringWriter();
//		 XMLWriter output = new XMLWriter(writer, format);
//		 
//		 try {
//			 File file = new File("rssNew.xml");
//			 output.output(document, new FileOutputStream(file));
//			output.write(document);
//			 writer.close();
//			 output.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	public static void createXml(String packagePath){
		
		try {
			// 1、创建document对象
			Document document = DocumentHelper.createDocument();
			// 2、创建根节点rss
			Element rss = document.addElement("mapper");
			rss.addAttribute("namespace", "com.sn.order.dao.LogisDao");
			// 4、生成子节点及子节点内容
			Element channel = rss.addElement("resultMap");
			channel.addAttribute("id", "BaseResultMap");
			channel.addAttribute("type", packagePath);
			Element esql = rss.addElement("sql");
			esql.addAttribute("id","Base_Column_List");
			// 5、设置生成xml的格式
			OutputFormat format = OutputFormat.createPrettyPrint();
			// 设置编码格式
			format.setEncoding("UTF-8");
			// 6、生成xml文件			
			File file = new File("D:/mapper.xml");
			XMLWriter writer = new XMLWriter(new FileOutputStream(file), format);
			// 设置是否转义，默认使用转义字符
			writer.setEscapeText(false);
			writer.write(document);
			writer.close();
			System.out.println("生成rss.xml成功");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("生成rss.xml失败");
		}
	}
	
	public static void getClassName(String name){
		
	}
}

