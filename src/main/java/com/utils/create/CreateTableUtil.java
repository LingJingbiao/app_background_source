package com.utils.create;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import com.utils.operation.FileUtil;

/**
 * 	数据库表生成器
 *  作者：凌景标
 *  时间：2019年4月20日上午11:06:29
 *
 */
public class CreateTableUtil {
	  static final String url="jdbc:mysql://localhost:3306/springboot?useUnicode=true&characterEncoding=utf-8";
	  static final String username="root";
	  static final String password="root";
	  public static void main(String[] args) throws Exception  
	    {  
	        Class.forName("com.mysql.jdbc.Driver");  
	        Connection conn = DriverManager.getConnection(url, username,password);  
	        Statement stat = conn.createStatement();  
	        conn = DriverManager.getConnection(url, username, password);  
	        stat = conn.createStatement();  
	        DatabaseMetaData mata= conn.getMetaData();
	       
	        createTable(conn,stat,mata);
	        //关闭数据库  
	        stat.close();  
	        conn.close();  
	    }  
	  
	   /**
		 * 创建表
	 * @param stat 
	 * @param conn 
	 * @param mata 
		 * @return
		 */
		public  static void createTable(Connection conn, Statement stat, DatabaseMetaData mata){
			String packages="com.base.sys.entity";
			
			 List<String> list=FileUtil.getPathName(packages);
			
			for(String lname:list){
				try {
				String cid="";
				Class clazz=Class.forName(packages+"."+lname);
				Field[] fields = clazz.getDeclaredFields();
				int f=0;
				String createSql="create table "+lname+" (";
		        //判断是否存在表  
				ResultSet set=mata.getTables(null, null, lname, null);
		        if(!set.next()){  
		            for (Field field : fields) {
		            	f++;
		            	String name =field.getName();
		            	name=CreateSqlUtils.toCharacter(name);
		            	if("id".equals(name)){
		            		cid=name;
		            	}
		            	if(field.getGenericType().toString().indexOf("String")!=-1){
		            		createSql+=name+" varchar(255)";
		            	}
		            	if(field.getGenericType().toString().indexOf("int")!=-1){
		            		createSql+=name+" int";
		            	}
		            	if(field.getGenericType().toString().indexOf("Integer")!=-1){
		            		createSql+=name+" int";
		            	}
		            	if(f<fields.length){
		            		createSql+=",";
		            	}
		            	
		            }
		            createSql+=",PRIMARY KEY ("+cid+"))";
		            //创建表 
		            stat.executeUpdate(createSql); 
		            System.out.println("表："+lname+"创建成功");
		            set.close();
		        }else{
		        	  System.out.println("表："+lname+"已存在");
		        }  
			} catch (Exception e) {
				System.out.println("表："+lname+"创建失败");
				e.printStackTrace();
			} 
			}
		}
}
