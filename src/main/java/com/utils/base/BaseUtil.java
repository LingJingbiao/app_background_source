package com.utils.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.alibaba.fastjson.JSON;
import com.business.mobile.entity.Publish;

/**
 * 基础获取工具类
 * 作者：凌景标
 * 时间：2019年5月10日下午2:41:28
 *
 */
public class BaseUtil {
	static final String FORMAT = "yyMMddHHmmssSSS";
	static DateFormat dateFormat = new SimpleDateFormat(FORMAT);
	
	/**
	 * 获取随机数
	 * @return
	 */
	public static String getUUID(){
		return dateFormat.format(new Date()).concat(String.valueOf(new Random().nextInt(99999)));

	}
	/**
	 * list object对象转化List实体类对象
	 * @return
	 */
	public static  List<Object> toEntityList(Object obj,List<Map> listmap){
		
		List<Object> entlist=new LinkedList<Object>();
		for(int i=0;i<listmap.size();i++){
			Object entity = JSON.parseObject(JSON.toJSONString(listmap.get(i)), obj.getClass());
			entlist.add(entity);
		}
		return entlist;
		
	}

}
