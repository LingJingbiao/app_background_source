package com.base.sys.service.impl;

import com.base.config.service.impl.BaseServiceImpl;
import com.base.sys.dao.DictDao;
import com.base.sys.entity.Dict;
import com.base.sys.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DictServiceImpl extends BaseServiceImpl implements DictService {
  @Autowired
  private DictDao modelDao;

  public Dict getDict(Dict model) {
    Dict pramModel=new Dict();
    try {pramModel= (Dict) super.queryModel(model);
    } catch (Exception e) {e.printStackTrace();
    }return pramModel;
  }
}
