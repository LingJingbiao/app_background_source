package com.base.sys.service.impl;

import com.base.config.service.impl.BaseServiceImpl;
import com.base.sys.dao.SysUserDao;
import com.base.sys.entity.SysUser;
import com.base.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends BaseServiceImpl implements SysUserService {
  @Autowired
  private SysUserDao modelDao;

  public SysUser getSysUser(SysUser model) {
    SysUser pramModel=new SysUser();
    try {pramModel= (SysUser) super.queryModel(model);
    } catch (Exception e) {e.printStackTrace();
    }return pramModel;
  }
}
