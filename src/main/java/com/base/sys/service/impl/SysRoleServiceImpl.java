package com.base.sys.service.impl;

import com.base.config.service.impl.BaseServiceImpl;
import com.base.sys.dao.SysRoleDao;
import com.base.sys.entity.SysRole;
import com.base.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRoleServiceImpl extends BaseServiceImpl implements SysRoleService {
  @Autowired
  private SysRoleDao modelDao;

  public SysRole getSysRole(SysRole model) {
    SysRole pramModel=new SysRole();
    try {pramModel= (SysRole) super.queryModel(model);
    } catch (Exception e) {e.printStackTrace();
    }return pramModel;
  }
}
