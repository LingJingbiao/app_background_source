package com.base.sys.service.impl;

import com.base.config.service.impl.BaseServiceImpl;
import com.base.sys.dao.SysResDao;
import com.base.sys.entity.SysRes;
import com.base.sys.service.SysResService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysResServiceImpl extends BaseServiceImpl implements SysResService {
  @Autowired
  private SysResDao modelDao;

  public SysRes getSysRes(SysRes model) {
    SysRes pramModel=new SysRes();
    try {pramModel= (SysRes) super.queryModel(model);
    } catch (Exception e) {e.printStackTrace();
    }return pramModel;
  }
}
