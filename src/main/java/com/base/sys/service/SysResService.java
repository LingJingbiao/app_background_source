package com.base.sys.service;

import com.base.config.service.BaseService;
import com.base.sys.entity.SysRes;

public interface SysResService extends BaseService {
  SysRes getSysRes(SysRes model);
}
