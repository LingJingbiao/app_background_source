package com.base.sys.service;

import com.base.config.service.BaseService;
import com.base.sys.entity.SysUser;

public interface SysUserService extends BaseService {
  SysUser getSysUser(SysUser model);
}
