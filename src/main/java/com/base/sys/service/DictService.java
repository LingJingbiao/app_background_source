package com.base.sys.service;

import com.base.config.service.BaseService;
import com.base.sys.entity.Dict;

public interface DictService extends BaseService {
  Dict getDict(Dict model);
}
