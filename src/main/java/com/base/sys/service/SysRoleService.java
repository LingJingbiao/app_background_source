package com.base.sys.service;

import com.base.config.service.BaseService;
import com.base.sys.entity.SysRole;

public interface SysRoleService extends BaseService {
  SysRole getSysRole(SysRole model);
}
