package com.base.sys.controller;

import com.base.config.entity.Pages;
import com.base.sys.entity.SysRes;
import com.base.sys.service.SysResService;
import java.lang.String;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 代码生成器 
 * */
@Controller
@RequestMapping("mb/sysres")
public class SysResController {
  @Autowired
  private SysResService controllerService;

  @RequestMapping("save")
  @ResponseBody
  public Map saveorupdate(SysRes model) {
    Map res=controllerService.saveorupdate(model);
    return res;
  }

  @RequestMapping("pageList")
  @ResponseBody
  public Pages pageList(SysRes model, Pages pages) {
    pages=controllerService.PageList(model, pages);
    return pages;
  }

  @RequestMapping("delete")
  @ResponseBody
  public String delete(SysRes model, String ids) {
    String res=controllerService.delete(model, ids);
    return res;
  }
}
