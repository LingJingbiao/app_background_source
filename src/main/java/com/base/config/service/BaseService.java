package com.base.config.service;

import java.util.List;
import java.util.Map;

import com.base.config.entity.Pages;



public interface BaseService {
	 
	 /**
	  * 保存或者更新
	  * @param sql
	  */
	 public Map<String,String> saveorupdate(Object obj);
	 
	 public Pages PageList(Object obj,Pages<Object> pages);
	 
	 public int count(Object obj,Pages<Object> pages);
	 
	 public String delete(Object obj,String ids); 
	 
	 public List queryList(Object obj);
	 
	 /**
	  * 查询关联id
	  * @param obj
	  * @param ids
	  * @return
	  */
	 public List queryIds(Object obj);
	 /**
	  * 获取实体类数据
	  * @param obj
	  * @return
	  */
	 public Object queryModel(Object obj);

	 /**
	  * 自定义sql查询
	  * @param sql
	  * @return
	  */
	 public List selectBySql(Object obj,String sql);
	 
}
