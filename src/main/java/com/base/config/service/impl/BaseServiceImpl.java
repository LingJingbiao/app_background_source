package com.base.config.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.config.dao.BaseDao;
import com.base.config.entity.Pages;
import com.base.config.service.BaseService;
import com.utils.base.BaseUtil;
import com.utils.create.CreateSqlUtils;

@Service
public class BaseServiceImpl  implements BaseService {

	 @Autowired
	 private BaseDao dao;

	 @Transactional
	public Map<String,String> saveorupdate(Object obj) {
		String res="2000";
		Map<String,String> map=new HashMap<String,String>();
			try {
				Map<String,String>resmap =CreateSqlUtils.saveorUpdateSql(obj);
				String sql=resmap.get("sql");
				dao.selectBySql(sql);
				String modelId=resmap.get("id");
				map.put("modelId", modelId);
			} catch (Exception e) {
				res="5000";
				e.printStackTrace();
			}
		map.put("res",res);
		return map;
		
	}

	public Pages PageList(Object obj, Pages<Object> pages) {
		List emtitylist=new ArrayList();
			try {
				String	countsql = CreateSqlUtils.getCount(obj, pages);
				List<Map> countmap=dao.selectBySql(countsql);
				String sql = CreateSqlUtils.queryList(obj, pages);
				List<Map> datemap=dao.selectBySql(sql);
				emtitylist=BaseUtil.toEntityList(obj, datemap);
				pages.setList(emtitylist);
				pages.setTotalCount(Integer.valueOf(countmap.get(0).get("totalCount").toString()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		return pages;
	}

	public int count(Object obj, Pages<Object> pages) {
		return 0;
	}
	@Transactional
	public String delete(Object obj, String ids) {
		Map<String, Object> map = new HashMap<String,Object>();
		String res="2000";
		try {
			String[] idssz= ids.split(",");
			String name=CreateSqlUtils.getClazzName(obj);
			map.put("table", name);
			map.put("ids", idssz);
			dao.delete(map);
		} catch (Exception e) {
			res="5000";
			e.printStackTrace();
		}
		return res;
	}

	public List queryList(Object obj) {
		List emtitylist=new ArrayList();
		 try {
			 String sql = CreateSqlUtils.queryList(obj,null);
			 List<Map> datemap=dao.selectBySql(sql);
			 emtitylist=BaseUtil.toEntityList(obj, datemap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emtitylist;
	}

	public Object queryModel(Object obj) {
		Object Object=new Object();
		if(queryList(obj).size()==1){
			Object=queryList(obj).get(0);
		}
		return Object;
	}

	public List selectBySql(Object obj,String sql) {
		List emtitylist=new ArrayList();
		List<Map> datemap=dao.selectBySql(sql);
		 emtitylist=BaseUtil.toEntityList(obj, datemap);
		return emtitylist;
	}

	public List queryIds(Object obj) {
		List emtitylist=new ArrayList();
		 try {
			String ids=CreateSqlUtils.getValue(obj, "Id");
			String sql = CreateSqlUtils.getIdsSql(obj,ids);
			List<Map> datemap=dao.selectBySql(sql);
			 emtitylist=BaseUtil.toEntityList(obj, datemap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return emtitylist;
	}

	
}
