package com.base.config.entity;

import java.util.List;
import java.util.Map;

/**
 * 分页实体
 * 作者：凌景标
 * 时间：2019年4月17日下午3:30:30
 *
 */
public class Pages<T>{
	/**
	 * 当前页数
	 */
	private int page=0;
	/**
	 * 总页数
	 */
	private int totalPage=10;  
	/**
	 * 总记录数
	 */
	private int totalCount;
	/**
	 * 每页显示的记录数
	 */
	private int limit;   
	/**
	 * 查询参数
	 */
	private Map<String,Object> param;
	/**
	 * 每页显示数据记录的集合；
	 */
	private List list;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Map<String, Object> getParam() {
		return param;
	}
	public void setParam(Map<String, Object> param) {
		this.param = param;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	
	
	
	

}
