package com.base.config.entity;

import java.util.Map;

/**
 *  返回信息类
 *  作者：凌景标
 *  时间：2019年5月8日下午9:53:39
 *
 */
public class ReturnMessage {
	private String mes;
	private Map<String,Object> mesMap;
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Map<String, Object> getMesMap() {
		return mesMap;
	}
	public void setMesMap(Map<String, Object> mesMap) {
		this.mesMap = mesMap;
	}
	
	

}
