package com.base.config.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.base.sys.entity.SysRole;
import com.base.sys.entity.SysUser;
import com.base.sys.service.SysResService;
import com.base.sys.service.SysRoleService;
import com.base.sys.service.SysUserService;

@Controller
public class LoginController {

	@Autowired
    private SysUserService userService;
	@Autowired
    private SysRoleService roleService;
	@Autowired
    private SysResService resService;
	
	/**
	 * 手机端登录
	 * @param user
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping("mb/login")
	@ResponseBody
	public String appLogin(SysUser user,HttpServletResponse response,HttpServletRequest request){	
		JSONObject jsonObject = new JSONObject();
		try {
			HttpSession session = request.getSession();
			Object loginUser = session.getAttribute("loginUser");
			if(loginUser==null){
				if(user.getUserName()!=null&&user.getPassWord()!=null){
					SysUser u =userService.getSysUser(user);
					if(u==null){
						//用户名或密码错误
						jsonObject.put("msg", 0);
					}else{
						SysRole roles=new SysRole();
						roles.setId(user.getRoleId());
						List<SysRole> role =roleService.queryIds(roles);
//						SysRes res =resService.getSysRes(new SysRes());
						session.setAttribute("loginUser",u);
					}			
				}else{
					jsonObject.put("msg", -1);
				}
			}else{
				jsonObject.put("msg", 1);
				jsonObject.put("loginUser", loginUser);
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.toJSONString();
		
	}
}
