package com.base.config.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BaseDao {
	    
	    public List<Map> selectBySql(@Param("sql") String  sql);
	    
	    public String delete(@Param("param") Map<String,Object>  param);
}
