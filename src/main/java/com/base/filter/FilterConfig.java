package com.base.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  拦截器
 *  作者：凌景标
 *  时间：2019年4月24日下午9:41:24
 *
 */
@Configuration
public class FilterConfig {
 
    @Bean
    public FilterRegistrationBean registFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LjFilter());
        registration.addUrlPatterns("/*");
        registration.setName("LjFilter");
        registration.setOrder(1);
        return registration;
    }
 
}