package com.base.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * 拦截器处理
 *  作者：凌景标
 *  时间：2019年4月24日下午9:41:42
 *
 */
public class LjFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {
 
    }
 
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	HttpServletRequest request=(HttpServletRequest) servletRequest;
    	String url=request.getRequestURI();
        String[] str=new String[]{"login","save","pageList","mb","image"};
        boolean tourl=false;
        for(String blj:str){
        	if(url.indexOf(blj)!=-1){
        		tourl=true;
        		System.out.println("没有被拦截"+url);
        		break;
        	}
        }
        if(tourl){//放行
        	filterChain.doFilter(servletRequest, servletResponse);
        }else{
        	HttpSession session = request.getSession();
			Object loginUser = session.getAttribute("loginUser");
			if(loginUser==null){
				System.out.println("请重新登录");
			}else{
				filterChain.doFilter(servletRequest, servletResponse);
			}
        }
    }
 
    public void destroy() {
    	System.out.println("destroy");
    }
}