package com.business.mobile.controller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.base.config.entity.Pages;
import com.business.mobile.entity.Publish;
import com.business.mobile.service.PublishService;
import com.utils.base.BaseUtil;
import com.utils.search.SearchUtils;

@Controller
@RequestMapping("mb/publish")
public class PublishController {
  @Autowired
  private PublishService controllerService;

  @RequestMapping("save")
  @ResponseBody
  public Map<String,String> saveorupdate(Publish model) {
	Map<String,String> res=controllerService.saveorupdate(model);
    return res;
  }

  @RequestMapping("pageList")
  @ResponseBody
  public Pages pageList(Publish model, Pages pages) {
    pages=controllerService.PageList(model, pages);
    return pages;
  }

  @RequestMapping("delete")
  @ResponseBody
  public String delete(Publish model, String ids) {
    String res=controllerService.delete(model, ids);
    return res;
  }
  
  @RequestMapping("upload")
  @ResponseBody
	public String uploadFile(@RequestParam MultipartFile  file,String mlName,String modelId){
	    Publish model=new Publish();
		String path =SearchUtils.getDictValue();
		String url="";
		if(!"".equals(mlName)){
			path +=mlName+File.separator;//创建一级目录
			url=File.separator+mlName+File.separator;
		}
		File tmpFile = new File(path);
		if(!tmpFile.exists()){
			tmpFile.mkdir();
		}
			String[] filName=file.getOriginalFilename().split("\\.");
			System.out.println(filName[filName.length-1]);
			String fileName = BaseUtil.getUUID()+"."+filName[filName.length-1];
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), new File(path,fileName));
				url +=File.separator+fileName;//返回图片路径
				model.setId(modelId);
				model.setImagePath(url);
				controllerService.saveorupdate(model);//保存图片
			} catch (IOException e) {
				e.printStackTrace();
			}
		return url;
		
	}
}
