package com.business.mobile.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.business.mobile.entity.Publish;

@Mapper
public interface PublishDao {
	    public void delete(String id);

	    public void insert(Publish model);

	    public void update(Publish model);
	    
	    public void selectBySql(@Param("sql") String  sql);
}
